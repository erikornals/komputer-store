import { formatCurr } from "./common.js"

const salaryElement = document.getElementById("el-salary");

let salary = 0;

export const setSalaryElement = () => {
    salaryElement.innerText = formatCurr(salary);
}

export const addSalary = () => {
    salary += 100;
    setSalaryElement();
}

export const withdrawSalary = () => {
    let amount = salary;
    salary = 0;
    setSalaryElement();
    return amount
}

export const addRepayLoanButton = (repayLoanButton) => {
    repayLoanButton.innerHTML = "Repay Loan";
    repayLoanButton.classList.add("button");
    document.getElementById("btn-work").after(repayLoanButton);
}

export const removeRepayLoanButton = (repayLoanButton) => {
    repayLoanButton.remove();
}