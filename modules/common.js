export const formatCurr = (amount) => new Intl.NumberFormat('no-NO', {style: 'currency', currency: 'NOK'}).format(amount);
