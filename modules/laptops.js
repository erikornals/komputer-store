import { formatCurr } from "./common.js"

const featuresList = document.getElementById("li-features");
const laptopImgElement = document.getElementById("img-laptop");
const laptopTitleElement = document.getElementById("el-laptop-title");
const laptopsSelect = document.getElementById("sel-laptops");
const descriptionElement = document.getElementById("el-description");
const priceElement = document.getElementById("el-price");

let selectedName;
let selectedPrice;
let laptops = [];
const url = "https://hickory-quilled-actress.glitch.me/";

export const getLaptopPrice = () => selectedPrice;
export const getLaptopName = () => selectedName;

//Display functions
const addSpecToFeatures = (spec) => {
    const featureElement = document.createElement("li");
    featureElement.innerText = spec;
    featuresList.appendChild(featureElement);
}

const addLaptopInfo = (laptopObj) => {
    featuresList.innerHTML="";
    laptopObj.specs.forEach(spec => addSpecToFeatures(spec));
    laptopImgElement.src = url+laptopObj.image;
    laptopTitleElement.innerText = laptopObj.title;
    descriptionElement.innerText = laptopObj.description;
    priceElement.innerText = formatCurr(laptopObj.price);
    //Assign selectedPrice
    selectedPrice = laptopObj.price;
    selectedName = laptopObj.title;
}

//API fetch functions
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsSelect.appendChild(laptopElement);
}

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(laptop => addLaptopToMenu(laptop));
    //Show initial selection
    addLaptopInfo(laptops[0])
}

export const handleLaptopSelect = (e) => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    addLaptopInfo(selectedLaptop);
}

export const fetchLaptopData = () => {
    fetch(url + "/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))
    .catch(function (error) {
        console.error('Fetch request failed: ', error);
    });
}