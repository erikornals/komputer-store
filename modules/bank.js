import { formatCurr } from "./common.js"

const balanceElement = document.getElementById("el-balance");
const outstandingLoanElement = document.getElementById("el-outstanding-loan");

let balance = 0;
let outstandingLoan = 0;

const sufficientBalance = (price) => price<=balance;

const outstandingLoanExists = () => outstandingLoan>0;

const setOutstandingLoanElement = () => {
    outstandingLoanElement.innerText = `Outstanding loan: ${formatCurr(outstandingLoan)}`;
}

export const setBalanceElement = () => {
    balanceElement.innerText = formatCurr(balance);
}

export const requestBankLoan = () => {
    if (outstandingLoanExists()) {
        alert("Outstanding loan must be paid back first.");
        return false
    }
    let loanRequest = Number(prompt("Loan amount: "));
    if (isNaN(loanRequest)){
        alert("Invalid input");
    } else if (loanRequest > 2*balance){
        alert("You may not get a loan more than double your bank balance.");
    } else if (loanRequest > 0){
        //Loan granted
        balance += Number(loanRequest);
        outstandingLoan += Number(loanRequest);
        setBalanceElement();
        setOutstandingLoanElement();
        return true
    }
    //Loan not granted
    return false
}

export const repayLoan = (payment) => {
    if (payment < outstandingLoan){
        //Insufficient funds to repay loan
        outstandingLoan -= payment;
        setOutstandingLoanElement();
        return false
    }
    //Loan fully repaid
    payment -= outstandingLoan;
    outstandingLoan = 0;
    outstandingLoanElement.innerText = "";
    balance += payment;
    setBalanceElement();
    return true
}

export const depositToBalance = (deposit) => {
    let loanRepaid = false;
    if (outstandingLoanExists()) {
        let deduction = Math.ceil(0.1*deposit);
        deposit -= deduction;
        loanRepaid = repayLoan(deduction);
    }
    balance += deposit;
    setBalanceElement();
    return loanRepaid;
}

export const purchaseBank = (price, productName) => {
    if (sufficientBalance(price)) {
        balance -= price;
        setBalanceElement();
        alert(`Congratulations on your purchase of ${productName}!`);
    } else {
        alert("Insufficient funds");
    }
}