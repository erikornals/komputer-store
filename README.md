# Komputer store

Komputer store is a website for buying komputers and managing your personal finances.

## Usage

Clone the repository and open it in VSCode. Add the Live Server extension in VSCode and, once installed, right click on ```index.html``` and open the website in your browser with 'Open with Live Server'.

## Contributing

Erik Alstad