import { setBalanceElement, depositToBalance, requestBankLoan, repayLoan, purchaseBank } from "./modules/bank.js"
import { setSalaryElement, addSalary, withdrawSalary, addRepayLoanButton, removeRepayLoanButton } from "./modules/work.js"
import { fetchLaptopData, handleLaptopSelect, getLaptopPrice, getLaptopName } from "./modules/laptops.js"

const workButton = document.getElementById("btn-work");
const bankButton = document.getElementById("btn-bank");
const getLoanButton = document.getElementById("btn-get-loan");
const repayLoanButton = document.createElement("button");
const laptopsSelect = document.getElementById("sel-laptops");
const buyButton = document.getElementById("btn-buy");

//Event handlers
const handleDepositBank = () => {
    let loanRepaid = depositToBalance(
        withdrawSalary()
    );
    if (loanRepaid) {
        removeRepayLoanButton(repayLoanButton);
    };
}

const handleGetLoan = () => {
    let loanGranted = requestBankLoan();
    if (loanGranted) {
        addRepayLoanButton(repayLoanButton);
    }
}

const handleRepayLoan = () => {
    let loanRepaid = repayLoan(
        withdrawSalary()
    );
    if (loanRepaid) {
        removeRepayLoanButton(repayLoanButton);
    };
}

const handleBuy = () => {
    purchaseBank(
        getLaptopPrice(),
        getLaptopName(),
    );
}

workButton.addEventListener("click", addSalary);
bankButton.addEventListener("click", handleDepositBank);
getLoanButton.addEventListener("click", handleGetLoan);
repayLoanButton.addEventListener("click", handleRepayLoan);
laptopsSelect.addEventListener("change", handleLaptopSelect);
buyButton.addEventListener("click", handleBuy);

//Setting default values
setBalanceElement();
setSalaryElement();

//Fetch data
fetchLaptopData();